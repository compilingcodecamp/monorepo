# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.3.0](https://gitlab.com/compilingcodecamp/monorepo/compare/v0.2.0...v0.3.0) (2019-08-02)


### Features

* **example:** add multiline markdown task example ([a772fec](https://gitlab.com/compilingcodecamp/monorepo/commit/a772fec))
* **frontend:** add frontend package ([1c32cef](https://gitlab.com/compilingcodecamp/monorepo/commit/1c32cef))





# [0.2.0](https://gitlab.com/compilingcodecamp/monorepo/compare/v0.1.0...v0.2.0) (2019-07-31)


### Features

* **graphql:** add first querytypes ([6fc2fa5](https://gitlab.com/compilingcodecamp/monorepo/commit/6fc2fa5))
* **graphql:** extend informationtype ([1850b9d](https://gitlab.com/compilingcodecamp/monorepo/commit/1850b9d))
* **graphql:** extend lessons type ([965c702](https://gitlab.com/compilingcodecamp/monorepo/commit/965c702))





# 0.1.0 (2019-07-25)


### Bug Fixes

* **version:** make version variable ([c5a59c1](https://gitlab.com/compilingcodecamp/monorepo/commit/c5a59c1))


### Features

* add conventional commits ([b5b3acf](https://gitlab.com/compilingcodecamp/monorepo/commit/b5b3acf))
* **graphql:** add graphql ([25f679e](https://gitlab.com/compilingcodecamp/monorepo/commit/25f679e))
