import { ObjectType, Field } from 'type-graphql';
import { LessonConfig } from '../../config/lessonconfig';

@ObjectType({
    description: 'represents the lesson type'
})
export class Lesson {
    @Field(type => String, {
        nullable: false,
        description: 'id to identify lessons with middlewares and the frontend'
    })
    id: string = '';

    @Field(type => String, {
        nullable: false,
        description: 'name for displaying lesson to user'
    })
    name: string = '';

    @Field(type => String, {
        nullable: false,
        description: 'task description written in markdown '
    })
    task: string = '';


    constructor(lesson: LessonConfig) {
        this.id = lesson.id;
        this.name = lesson.name;
        this.task = lesson.task;
    }
}