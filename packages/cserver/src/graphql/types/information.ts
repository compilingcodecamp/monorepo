import { ObjectType, Field } from 'type-graphql';
import { glob } from '../../globals';

@ObjectType({
    description: 'store information about the server instance'
})
export class Information {
    @Field(type => String, {
        nullable: false,
        description: 'course version'
    })
    version: string = glob.version;

    @Field(type => String, {
        nullable: false,
        description: 'course name'
    })
    name: string = glob.name;

    @Field(type => String, {
        description: 'cserver version'
    })
    cserver_version: string = glob.cserver_version
}