import { Resolver, Query } from 'type-graphql';
import { Lesson } from '../types/lesson';
import { glob } from '../../globals';

@Resolver(Lesson)
export class LessonResolver {
    @Query(returns => [Lesson])
    lessons() {
        const arr: Lesson[] = [];
        for(let lesson of glob.lessons) {
            arr.push(new Lesson(lesson))
        }
        return arr;
    }
}