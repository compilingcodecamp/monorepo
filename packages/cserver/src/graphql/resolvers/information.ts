import { Resolver, Query } from 'type-graphql';
import { Information } from '../types/information';

@Resolver(Information)
export class InformationResolver {
    @Query(returns => Information)
    information() {
        return new Information();
    }
}