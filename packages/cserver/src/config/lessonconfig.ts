export interface LessonConfig {
    /** unique lesson id */
    id: string;
    /** lesson name, e.g. 0.1 Variables */
    name: string;
    /** task in markdown code */
    task: string;
}