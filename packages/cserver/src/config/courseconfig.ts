import { LessonConfig } from './lessonconfig';

export interface CourseConfig {
    /**
     * course name
     */
    name: string;
    /**
     * course version
     */
    version: string;

    lessons: LessonConfig[];
};