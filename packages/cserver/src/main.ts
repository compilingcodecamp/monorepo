import 'reflect-metadata';
import express from 'express';
import graphQLHttp from 'express-graphql';
import { buildSchema } from 'type-graphql';
import cors from 'cors';

import { InformationResolver } from './graphql/resolvers/information';
import { loadGlobals } from './globals';
import { LessonResolver } from './graphql/resolvers/lesson';
import { CourseConfig } from './config/courseconfig';
import { exampleServer } from './exampleserver';

const PORT = 8001;

const Resolvers = [
    InformationResolver,
    LessonResolver,
];

export async function bootstrap(config: CourseConfig){

    // load globals
    loadGlobals(config);

    const schema = await buildSchema({
        resolvers: Resolvers,
        emitSchemaFile: './schema.gql',
    });
    
    const app = express();
    app.use('/api', cors() ,graphQLHttp({
        schema,
        graphiql: true,
    }));

    app.listen(PORT);
}


if(!module.parent) {
    bootstrap(exampleServer);
}
