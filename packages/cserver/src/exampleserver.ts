import { CourseConfig } from './config/courseconfig';

const task_1: string = 
`# no memories
the computer does not have a brain, but we have a temporary
storage, called RAM, where we can store data. But the RAM 
Adresses are quite cryptic, so the programmer uses aliases
for the adress, these are variables.
\`\`\`Javascript
const variable = 'hello world';
console.log(variable);
\`\`\`
`;

export const exampleServer: CourseConfig = {
    name: 'example server',
    version: '0.0.0',
    lessons: [
        {
            id: '0.1',
            name: 'Variables',
            task: task_1,
        },
        {
            id: '0.2',
            name: 'Loops',
            task: 'do while for exists'
        },
    ],
};