import { promises } from 'fs-extra';
import * as path from 'path';
import { CourseConfig } from './config/courseconfig';
import { LessonConfig } from './config/lessonconfig';
export let glob = {
    cserver_version: '-1.0.0',
    version: '-1.0.0',
    name: 'example server',
    lessons: <LessonConfig[]>[]
};

/**
 * load everything, thats globally needed,
 * like the version, package.json etc.
 */
export async function loadGlobals(config: CourseConfig) {
    // load package.json
    const package_json = JSON.parse((await promises.readFile(path.resolve(__dirname, '../package.json'))).toString());
    glob.cserver_version = package_json.version;
    glob.name = config.name;
    glob.version = config.version;
    glob.lessons = config.lessons;
}