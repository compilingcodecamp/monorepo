import React, { Component } from 'react';
import { request } from 'graphql-request';
import { lessonsQuery } from './lessons.query';

export interface ITaskProps {

}

export interface ITaskState {
    lessons: { id: string, name: string }[];
}
export class Task extends Component<ITaskProps, ITaskState> {

    constructor(props: ITaskProps) {
        super(props)
        this.state = {
            lessons: [],
        };
        request('http://localhost:8001/api', lessonsQuery).then(data => 
            this.setState({lessons: data.lessons})
        );
    }

    render() {
        const less = this.state.lessons.length === 0 ? <p>no lessons found</p> : this.state.lessons.map(val => (<p key={val.id}>{val.name}</p>))
        return(
            <div>
                <h3>Lessons</h3>
                { less }
            </div>
        )
    }

}