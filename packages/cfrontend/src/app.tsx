import React,{ Component } from 'react';
import { Task } from './components/lessons/lessons';

export interface IAppProps {

}

export interface IAppState {

}

export class App extends Component<IAppProps, IAppState> {

    constructor(props: IAppProps) {
        super(props);
    }

    render() {
        return(
            <Task />
        )
    }

}